# Fretes no Brasil

Uma rápida análise a partir de um conjunto de dados sobre fretes de produtos, com a proposta de gerar um relatório final em PDF com gráficos e pequenos parágrafos de texto. 

## Quais são os dados, e de onde vêm?

A fonte dos dados é uma planilha Excel, com informações acerca de fretes de produtos, com preços, locais, datas e outras identificações.

## Qual o problema presente aqui?

 Não há um problema específico apontado pelo responsável pela base de dados ou pelo instrutor do curso; a ideia é explorar a base de dados e extrair informações, gerar insights, construir visualizações e possibilitar a outros um maior conhecimento acerca dos dados, para que daí se tome uma decisão.

## Como a solução foi planejada?

A proposição era utilizar as funcionalidades do próprio Excel. Percebo de observação própria que bastante gente utilizaria Power Query e tabelas dinâmicas; eu provavelmente faria o mesmo. Porém, encorajado pelo criador do projeto, apenas funções, como soma, média, contagem, seriam utilizadas. Por fim, o próprio Excel seria utilizado também para gerar um relatório em PDF.

## E a solução em si, como foi executada?

A partir da primeira aba com os dados, foi feita uma cópia para começar a limpeza de dados. Ali, a exibição das datas (do pedido, da compra, do despacho e da entrega) e dos preços de frete foi corrigida, a coluna Sistema foi retirada. Também criei uma coluna de mês, para isolar esse dado.

Para a análise, uma nova aba foi criada e utilizamos funções para calcular as métricas desejadas. Total de frete geral; por estado, o valor total, a média, o maior e o menor, e ainda a quantidade de fretes, e a variação de dias para entrega - o mais rápido, o mais demorado, e a média; por mês, o total, a média, a quantidade; por SKU, valores total e médio de frete, e quantidade de fretes; e a porcentagem por forma de pagamento. 

Uma nova aba com as métricas calculadas, e gerei gráficos. Aqui fiquei um pouco às voltas com a forma de representar: barra, pizza/rosca, linha e outros, até encontrar uma solução visualmente agradável. Por fim, foi gerado um relatório em PDF com pequenos parágrafos descrevendo cada gráfico.

## Dificuldades? Trabalhos futuros?

Essa foi minha primeira análise utilizando Excel. Estou habituado com programação (Python, em especial), e embora o Excel seja bastante poderoso, tive receio de esbarrar em alguma limitação na análise. Não foi o caso; para o dataset, as funções cobriram boa parte do que se propôs a analisar. A minha maior limitação foi na visualização: minha própria falta de noção de storytelling e estética. O receio se transformou na dúvida se os gráficos estão legíveis e não muito congestionados. 

Outra dificuldade foi a própria geração do PDF. A ideia inicial era utilizar o próprio Excel para gerar um PDF rápido, porém não se tornou nada rápido. A quebra de página é frequentemente confusa e adicionar ou retirar texto muda toda a diagramação. Preferi utilizar o Word mesmo, para não perder tempo demais. 

Para o futuro, achei interessante trabalhar assim com Excel. Embora eu não perceba mais tantas vagas exigindo Excel em alta prioridade, ainda é bem utilizado para uma análise rápida. Penso que posso fazer algo explorando funcionalidades mais avançadas, como tabela dinâmica, Power Query, ou mesmo código. Além do Copilot integrado nas ferramentas Microsoft, e o próprio Python.