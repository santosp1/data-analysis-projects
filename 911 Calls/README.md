# 911 Calls

Uma análise exploratória do dataset contendo informações de chamadas ao 911 no condado de Montegomery. Esse projeto foi concluído como parte de um curso online de ciência de dados.

## Quais são os dados, e de onde vêm?

A base de dados possui informações do 911 do condado de Montgomery. Os dados são públicos, disponíveis em um kernel do Kaggle no formato csv. 

## Qual o problema aqui?

O projeto é parte de um curso online, e portanto algumas das ferramentas já estavam incluídas, ou estavam implícitas. Não há um problema específico apontado pelo responsável pela base de dados ou pelo instrutor do curso; a ideia é explorar a base de dados e extrair informações, gerar insights, construir visualizações e possibilitar a outros um maior conhecimento acerca dos dados, para que daí se tome uma decisão. 

## Como a solução foi planejada?

A proposição é construir a solução a partir de um Jupyter Notebook, utilizando linguagem Python e as bibliotecas Numpy, Pandas, Matplotlib e Seaborn. Esse conjunto de ferramentas possibilita, dos dados, o carregamento, o pré-processamento (limpeza, transformação, preparação), bem como modelagem e visualização. A partir das visualizações, vêm a geração de insights, a procura por informações extras e as interpretações dos fenômenos encontrados. Outras ferramentas e processos podem ser utilizados para contribuir para esse objetivo.

Essa escolha de linguagem de programação e das ferramentas conta também como premissa de projeto, pois sendo considerados parte do aprendizado do curso não seria ideal andar por um caminho diferente.

## E a solução em si, como foi executada?

Como a proposição era fazer um Jupyter Notebook, então a análise exploratória traria descobertas a partir de cada verificação dos dados. A solução foi construída em blocos de linhas de código, intercalados com a visualização resultante (seja uma imagem, seja outro dado construído e agregado), a interpretação do que foi executado e, caso se aplique, o impacto geral para o projeto, ou o seguimento para a próxima seção.

A partir do que foi carregado do csv num DataFrame Pandas, a análise tanto criou visualizações quanto manipulou e transformou dados. De início foi verificado que haviam dados faltantes, porém ao longo da análise isso não demonstrou ser um impedimento. Desmembrou-se colunas em outras e criou-se novas -- como criar `reason` ("categoria") a partir de `title` ("descrição completa da chamada") e colunas de data, hora, dia da semana e mẽs a partir de `timeStamp`.

Foram criados gráficos de barras pra quantificar `reason` em números gerais, bem como observar o total de chamadas (separados por categoria) por dia da semana, por mês do ano, por ano e por todos os meses do dataset. Também foram desenhados gráficos de linha para observar as chamadas por mês (tanto o total quanto separando por categoria), tal qual respectivas regressões lineares de análise de tendência. Por fim, investigou-se outliers ao desenhar gráficos lineares baseados em dias, obtendo achados interessantes (em particular, 3 grandes dias de eventos climáticos severos), e heatmaps e clustermaps para diferentes visualizações.

## Dificuldades? Trabalhos futuros?

Na publicação desta análise, a base de dados não vinha sendo mais atualizada em alguns anos. Caso seja possível acessar dados atualizados de outra maneira (com outra pessoa fazendo upload, por exemplo), seria possível executar uma análise mais longa, isto é, em uma maior faixa temporal de registros. 

Também seria possível utilizar outras bibliotecas no lugar das existentes. Por exemplo, Polars ao invés de Pandas. Ou mesmo outra linguagem de programação, como o R, porém isso fugiria da proposta inicial do projeto.