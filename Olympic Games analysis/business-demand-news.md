"Hi! Hope you're doing fine.

As a data analyst working at a news company, you are asked to visualize data that will help readers understand how countries have performed historically in the Olympic Games.

You also know that there is an interest in details about the competitors, so if you find anything interesting then don't hesitate to bring that in also.

The main task is still to show historical performance for different countries, with the possibility to select your own country."

* * * 

asked to visualize data  
help readers understand  
countries  
performed historically  
interest in details about the competitors  
aything interesting  
select your own country  

Dashboard  
Linguagem simplificada, mas informativa  
Alguns países mudaram de nome. Os dados abordam isso?  
Há jogos de verão e de inverno? Apenas verão? Posso ver no SQL e no quadro  
Todos os atletas estão na base de dados? Incluindo esportes coletivos?  
Medalha por esporte ou por atleta? A database faz essa distinção?   
Outros? Faixa etária? Gênero?   
