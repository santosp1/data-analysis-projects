SELECT [ID]
      ,[Name] as 'Competitor'
      ,case when [Sex] = 'M'then 'MALE' when [Sex] = 'F' then 'FEMALE'
	   end as 'Gender'
      ,[Age]
	  ,case when [Age] < 18 then 'Less than 18'
			when [Age] between 18 and 21 then '18-21'
			when [Age] between 22 and 25 then '22-25'
			when [Age] between 26 and 29 then '26-29'
			when [Age] >= 30 then '30 and over'
		end as [Age group]
      ,[Height]
      ,[Weight]
      ,[NOC] as 'Nation code'
      ,[Games]
	  ,left(Games,4) as 'Year'
	  ,right(Games,6) as 'Season'
      --,[City]
      ,[Sport]
      ,[Event]
      ,case when [Medal] = 'NA' then 'No medal' else [Medal] end as [Medal]
  FROM [olympic_games].[dbo].[athletes_event_results]
