# Olympic Games analysis

Uma análise descritiva de um dataset contendo participantes e medalhistas dos Jogos Olímpicos da Era Moderna, desde a sua incepção em 1896 até os Jogos Olímpicos do Rio de Janeiro, em 2016. 

## Quais são os dados, e de onde vêm?

Os dados vieram de um arquivo .bak: um backup de uma base de dados do SQL Server.

## Qual o problema presente aqui?

Há um problema de negócio, uma demanda enviada a partir do gerente de redação de uma empresa de mídia e imprensa. A demanda e um brainstorm/overview dela estão no arquivo business-demand-news.

Em resumo, há um pedido para gerar uma visualização de dados que ajudariam leitores a entender desempenho histórico dos países nos Jogos Olímpicos, com a possibilidade de selecionar seu próprio país. Detalhes adicionais sobre competidores tambem são interessantes, bem como outros insights que o analista encontre.

## Como a solução foi planejada?

 Como a fonte de dados é um backup do SQL Server, isso já implica o uso desse gerenciador de banco de dados. O objetivo principal é a visualização de dados, então é necessária uma ferramenta voltada para isso. O Power BI foi escolhido, permanecendo em uma stack Microsoft. A solução envolveria o habitual pré-processamento (limpeza, transformação, preparação) dos dados, selecionando as colunas relevantes, transformando o que fosse necessário, e gerando um SQL que chegasse no resultado desejado. A partir daí, seria extraído um .csv para o Power BI e criar nele a visualização, e se necessário usar DAX para refinar os valores.

## E a solução em si, como foi executada?

![dashboard olympic games analysis](image-1.png)

O backup do banco de dados foi carregado no SQL Server copiando o arquivo para o diretório onde ele pode ser acessado (varia de acordo com a versão do SQL Server Management Studio). Ali, queries SQL investigaram a base de dados, que possui duas tabelas, sendo uma delas apenas o código da nação e o subsequente nome, e outra tabela com todos os outros dados: nome do atleta, id, código da nação, esporte, a categoria que disputou, a Olimpíada que participou, a medalha recebida (ou não), e outros.

O .csv gerado foi importado dentro do Power BI. À esquerda do dashboard há slicers para seleção em dropdown de temporada (Olimpíádas de verão ou inverno), ano dos jogos, país, competidor e esporte. A primeira variável tem itens excludentes, então sempre um, e somente um, está ativo; das outras opções nas outras variáveis podem ser todos, alguns ou nenhum.

Utilizamos DAX para calcular o total de competidores, de medalhistas, e a separação por gênero, e incluímos em cards e gráficos rosca. Gráficos de barra mostram as medalhas por esporte, por competidor e por faixa etárea, e um gráfico de área mostra a linha do tempo do total de medalhas desde as primeiras Olimpíádas, separando por medalha e por ano.

## Dificuldades? Trabalhos futuros?

A database é incompleta em diferentes âmbitos. Estamos em 2024, já tivemos as Olimpíadas de Inverno de 2018 e 2022, e as Olimpíadas de Verão de 2020 (em 2021, devido à pandemia) e de 2024, mas os dados param em 2016. Além disso, nos esportes coletivos nem todos os atletas estão presentes como medalhistas. Alguns nomes também têm caracteres faltantes, em especial letras com acentos.

Uma dificuldade para essa primeira iteração foi decidir pelo número de medalhas. No quadro geral de medalhas a atribuição é por evento. Então, por exemplo, o futebol de campo dá uma medalha. Mas na atual versão cada atleta registrado na base como medalhista conta uma medalha para o seu país.

Em uma futura versão, cada evento será filtrado para corrigir tais diferenças. Tanto as atribuições de medalha por país, quanto a individualidade de cada atleta medalhista. Serão contados de forma diferente, e exibidos no dashboard em lugares diferentes.

A primeira tabela da base de dados não se mostrou, em princípio, realmente útil pois as muitas mudanças de nome de país dificultam a atribuição hoje. Por exemplo, um atleta da antiga Iugoslávia ou da União Soviética não possui, nesta base, informações (como cidade de origem) que o liguem ao país atual e, apenas caso seja desejado, refazer a série histórica de medalhas.

Explorar outras bases de dados podem ajudar com essa questão, bem como a correção de medalhas por esporte e atleta. Fórmulas DAX também são uma opção para tratar isso direto no Power BI.