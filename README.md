# Data Analysis Projects

Este repositório é uma coleção de projetos de Análise de Dados (e Ciência de Dados) que eu tenho trabalhado, usando Python, utilizando análises estatísticas e visualização para extrair insights dos dados. Para cada pasta nesse diretório raiz há um projeto com seu próprio README e arquivos necessários.

This repo is a collection of Data Analysis (and Data Science) projects that I've worked on, using Python, utilizing statistical analysis and visualization to extract insights from data. For each folder on the main directory there is a project, with its own README and necessary files.

___

**911 Calls** - Python + Jupyter -- Uma análise exploratória do dataset contendo informações de chamadas ao 911 no condado de Montegomery. Esse projeto foi concluído como parte de um curso online de ciência de dados. | An exploratory analysis of the dataset (from Kaggle) containing info from Montegomery County's 911 calls. I completed this capstone project as part of an online data science course.

**Olympic Games analysis** - SQL + PowerBI -- Uma análise descritiva de um dataset contendo participantes e medalhistas dos Jogos Olímpicos da era Moderna, desde seu início em 1896 até os Jogos de 2016 no Rio de Janeiro. Este projeto foi sugestão de um vídeo no YouTube. | A descriptive analysis of a dataset containing participants and medal winners from the Modern Olympic Games, from their inception in 1896 to the 2016 Olympic Games in Rio de Janeiro. This project is a suggestion by a YouTube video.

___

## Novo/New:

**Fretes no Brasil** (in portuguese) - Excel -- Uma rápida análise a partir de um conjunto de dados sobre frete de produtos, com a proposta de gerar um relatório final em PDF com gráficos e pequenos parágrafos de texto. Este projeto foi sugestão de um vídeo no YouTube. | A quick analysis based on a dataset about product shipping, with the aim of generating a PDF report with graphs and short text paragraphs. This project is a suggestion by a YouTube video.