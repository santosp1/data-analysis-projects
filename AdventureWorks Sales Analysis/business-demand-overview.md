Business Demand Overview

Reporter:
Value of change:
Necessary Systems:
Other Relevant Info:

| As a (role) | I want (request/demand) | So that I (user value) | Acceptance Criteria |
|----|----|----|----|
| Sales Manager | An overview of Internet sales | Can better observe which products sell more to which customers |A PowerBI dashboard that updates data once a day| 
|Sales Representative| A detailed view of Internet sales by customers | Can see which customers are buying more, and who we might sell more to | A PowerBI dashboard that allows me to filter data by customer |
| Sales Representative| A detailed view of Internet sales by product | Can see which products are selling the most | A PowerBI dashboard that allows me to filter data by product |
| Sales Manager| An overview of Internet sales| I can track sales over time against budget| A PowerBI dashboard with charts and KPIs against budget|


Steven  - Gerente de Vendas:

Oi!

melhorar nossos relatórios de vendas
dashboards visuais.
o quanto vendemos
quais produtos
quais clientes
ao longo do tempo.
cada vendedor trabalha com produtos e clientes diferentes
em relação ao orçamento
comparar nossos valores com o desempenho.
2021, 2020, 2019

Demanda de Negócio - Visão Geral

Informante: Steven
Valor na mudança: Dashboards visuais e melhoramento no informe das vendas
Sistemas necessários: PowerBI, CRM
Outras informações relevantes: O orçamento para 2021 está em uma planilha Excel

| Como um (papel) | Eu quero (requisição/demanda) | Para que eu (valor de usuário) | Critérios de Aceitação |
|----|----|----|----|
|Gerente de Vendas| Um visualização geral das vendas de internet | Possa observar melhor quais produtos vendem mais para quais clientes | Um dashboard do PowerBI que atualiza dados uma vez ao dia| 
|Representante de Vendas| Uma visualização detalhada das vendas de internet por clientes | Possa verificar os clientes que compram mais, e para quem poderemos vender mais | Um dashboard do PowerBI que me permita filtrar dados por cliente|
|Representante de Vendas| Uma visualização detalhada das vendas de internet por produto | Possa verificar os produtos que vendem mais | Um dashboard do PowerBI que me permita filtrar dados por produto|
| Gerente de Vendas| Uma visualização geral das vendas de internet| Possa seguir as vendas ao longo do tempo comparando com o orçamento| Um dashboard do PowerBI com gráficos e KPIs comparando com o orçamento|

