Steven  - Sales Manager:

Hi!

I hope you are doing well. We need to improve our internet sales reports and want to move from static reports to visual dashboards.

Essentially, we want to focus it on how much we have sold of what products, to which clients and how it has been over time.

Seeing as each sales person works on different products and customers it would be beneficial to be able to filter them also.

We measure our numbers against budget so I added that in a spreadsheet so we can compare our values against performance. 

The budget is for 2021 and we usually look 2 years back in time when we do analysis of sales.

Let me know if you need anything else!

// Steven

* * *

Oi!

Espero que você esteja indo bem. Precisamos melhorar nossos relatórios de vendas pela internet e queremos passar de relatórios estáticos para dashboards visuais.

Essencialmente, queremos focar no quanto vendemos de quais produtos, para quais clientes e como tem sido ao longo do tempo.

Visto que cada vendedor trabalha com produtos e clientes diferentes, seria benéfico poder filtrá-los também.

Medimos nossos números em relação ao orçamento, então acrescentei isso em uma planilha para que possamos comparar nossos valores com o desempenho. 

O orçamento é para 2021 e costumamos comparar com 2 anos atrás quando fazemos análises de vendas.

Me avisa se precisar de mais alguma coisa!

// Steven